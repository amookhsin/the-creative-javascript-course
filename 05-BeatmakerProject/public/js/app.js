class Select {
    constructor(id) {
        this._el = document.getElementById(id);
        this.value = this._el.value;
    }

    onChange(fn) {
        this._el.addEventListener('change', e => fn(e.target.value));
    }
}

class MuteButton {
    constructor(id) {
        this._el = document.getElementById(id);
        this._muteListener = null;
        this._unmuteListener = null;

        // Initiate
        this._el.addEventListener('click', e => {
            this.toggle();

            if (this.isMuted()) {
                if (this._muteListener) {
                    this._muteListener(e);
                }
            } else {
                if (this._unmuteListener) {
                    this._unmuteListener(e);
                }
            }
        });
    }

    isMuted() {
        return this._el.classList.contains('muted');
    }

    toggle() {
        this._el.classList.toggle('muted');
    }

    setMuteListener(fn) {
        this._muteListener = fn;
    }

    removeMuteListener() {
        this._muteListener = null;
    }

    setUnmuteListener(fn) {
        this._unmuteListener = fn;
    }

    removeUnmuteListener() {
        this._unmuteListener = null;
    }
}

class Controller {
    constructor(select, muteButton) {
        this.select = select;
        this.muteButton = muteButton;
    }
}

class BoxPad {
    constructor(id) {
        /**
         * HTMLElement
         */
        this._el = this.el = document.getElementById(id);
        /**
         * Array[HTMLElement]
         */
        this._pads = [...this.el.childNodes].filter(el => el.nodeType != 3);

        this._pads.forEach((el, bit) => {
            el.addEventListener('click', e => this._clicked(e.target, bit));
        });
    }

    _clicked(el, bit) {
        if (el.classList.contains('selected')) {
            this.unselect(bit);
        } else {
            this.select(bit);
        }
    }

    initiate() {
        let cls = ['playing'];
        this._pads.forEach(el => {
            el.classList.remove(...cls);
        });
    }

    reset() {
        let cls = ['playing', 'selected'];
        this._pads.forEach(el => {
            el.classList.remove(...cls);
        });
    }

    isSelected(bit) {
        return this._pads[bit].classList.contains('selected');
    }

    length() {
        return this._pads.length;
    }

    select(bit) {
        this._pads[bit].classList.add('selected');
    }

    unselect(bit) {
        this._pads[bit].classList.remove('selected');
    }

    playing(bit) {
        let class_name = 'playing';
        this.initiate();
        if (!this.isSelected(bit)) {
            return false;
        }

        this._pads[bit].classList.add(class_name);
        return true;
    }
}

class AudioPlayer {
    constructor(id, src = null) {
        this._el = document.getElementById(id);
        if (!this._el) {
            const audio = document.createElement('audio');
            audio.id = id;
            document.getElementsByTagName('body')[0].appendChild(audio);
            this._el = audio;
        }

        this.source(src);
    }

    mute() {
        this._el.muted = true;
    }

    unmute() {
        this._el.muted = false;
    }

    play(start = true) {
        if (start) this._el.currentTime = 0;
        this._el.play();
    }

    pause() {
        this._el.pause();
    }

    stop() {
        this._el.currentTime = 0;
        this._el.pause();
    }

    source(src) {
        this._el.src = src;
    }
}

class DrumKit {
    constructor(duration, ...args) {
        this._duration = duration;
        this._proportion = 1;
        this._tracks = {};
        this._interval = null;
        this._current_bit = 0;

        // Initiate
        args.forEach(track => {
            const controller = new Controller(new Select(`${track}-select`), new MuteButton(`${track}-mute`));
            const pad = new BoxPad(`${track}-pad`);
            const player = new AudioPlayer(`${track}-audio`, controller.select.value);
            controller.select.onChange(value => player.source(value));
            controller.muteButton.setMuteListener(e => player.mute());
            controller.muteButton.setUnmuteListener(e => player.unmute());

            this._tracks[track] = {
                'controller': controller,
                'pad': pad,
                'player': player
            };
        });
    }

    isPlaying() {
        return this._interval != null;
    }

    proportion(proportion) {
        this._proportion = proportion;
    }

    play() {
        this._interval = setInterval(() => {
            for (let track in this._tracks) {
                let padbox = this._tracks[track].pad;
                let audio_player = this._tracks[track].player;

                if (padbox.playing(this._current_bit % padbox.length())) {
                    audio_player.play();
                }
            }

            this._current_bit++;
        }, this._duration / this._proportion);
    }

    stop() {
        if (this._interval) {
            clearInterval(this._interval);
            this._interval = null;

            for (let track in this._tracks) {
                let padbox = this._tracks[track].pad;
                let audio_player = this._tracks[track].player;
                audio_player.stop();
                padbox.initiate();
            }
        }
    }
}


drumKit = new DrumKit(1000, 'kick', 'snare', 'hihat');

// Selectors
play_btn = document.getElementById('drum-play');
tempo = document.getElementById('tempo');

// Listener
play_btn.addEventListener('click', e => {
    if (drumKit.isPlaying()) {
        e.target.textContent = "Start";
        drumKit.stop();
    } else {
        e.target.textContent = "Stop";
        drumKit.play();
    }
});

tempo.addEventListener('change', e => {
    drumKit.proportion(e.target.value);
    drumKit.stop();
    drumKit.play();
});