class Slider {
    /**
     * Create new instance
     * 
     * @param {HTMLElement} el
     */
    constructor(el) {
        this._el = el;
        this._listener = null;
        /** chroma|null */
        this._color = null;

        // Selectors
        this._btn_close = this._el.getElementsByClassName('close')[0];
        this._input_hue = this._el.getElementsByClassName('input-hue')[0];
        this._input_hue.style.backgroundImage = `linear-gradient(to right, 
            rgb(204, 75, 75), 
            rgb(204, 204, 75), 
            rgb(75, 204, 75), 
            rgb(75, 204, 204), 
            rgb(75, 75, 204), 
            rgb(204, 75, 204), 
            rgb(204, 75, 75))`;
        this._input_saturate = this._el.getElementsByClassName('input-saturation')[0];
        this._input_bright = this._el.getElementsByClassName('input-brightness')[0];

        // Initiate
        this._btn_close.addEventListener('click', e => this.close(e));
        this._input_hue.addEventListener('input', e => this._onChangeHandler(e));
        this._input_bright.addEventListener('input', e => this._onChangeHandler(e));
        this._input_saturate.addEventListener('input', e => this._onChangeHandler(e));
    }

    _onChangeHandler(e) {
        this._color_updated();

        if (this._listener) {
            this._listener(this._color);
        }
    }

    /**
     * Update saturation input background
     */
    _update_st_bg() {
        const scaleSat = chroma.scale([
            this._color.set('hsl.s', this._input_saturate.min),
            this._color,
            this._color.set('hsl.s', this._input_saturate.max)
        ]);
        this._input_saturate.style.backgroundImage = `linear-gradient(to right, ${scaleSat(this._input_saturate.min)}, ${scaleSat(this._input_saturate.max)})`;
    }

    /**
     * Update brightness input background
     */
    _update_br_bg() {
        const scaleBright = chroma.scale([
            'black',
            this._color.set('hsl.l', (this._input_bright.min + this._input_bright.max) / 2),
            'white'
        ]);
        this._input_bright.style.backgroundImage = `linear-gradient(to right, 
            ${scaleBright(this._input_bright.min)}, 
            ${scaleBright((this._input_bright.min + this._input_bright.max) / 2)}, 
            ${scaleBright(this._input_bright.max)})`;
    }

    /**
     * Update color property from inputs fields
     */
    _color_updated() {
        let hsl = {
            hue: Number(this._input_hue.value),
            saturation: Number(this._input_saturate.value),
            brightness: Number(this._input_bright.value)
        };
        this._color = chroma(hsl.hue, hsl.saturation, hsl.brightness, 'hsl');

        this._update_st_bg();
        this._update_br_bg();
    }

    /**
     * Open slide menu to get adjustment related to the color
     * 
     * @param {chroma} color  current color
     */
    adjust(color) {
        this._color = color;

        this._input_hue.value = this._color.hsl()[0];
        this._input_saturate.value = this._color.hsl()[1];
        this._input_bright.value = this._color.hsl()[2];

        this._color_updated(color);

        this._el.classList.add('show');
    }

    /**
     * Set listener
     * 
     * @param {function} fn fn(adjust)
     */
    setListener(fn) {
        this._listener = fn;
    }

    /**
     * Close slider menu
     */
    close() {
        this._el.classList.remove('show');
    }
}

class Color {
    /**
     * Create new instance
     * 
     * @param {HTMLElement} el 
     */
    constructor(el) {
        this._el = el;
        this._listener = null;
        /** chroma */
        this.color = chroma('red');

        // Selectors
        this._txt_color = this._el.getElementsByClassName('value')[0];
        this._txt_color.addEventListener('click', e => {
            navigator.clipboard.writeText(e.target.textContent);
            let popup = new CopyPopup();
            popup.open();
            setTimeout(() => popup.close(), 1500);
        });
        this._btn_adjust = this._el.getElementsByClassName('adjust')[0];
        this._btn_lock = this._el.getElementsByClassName('lock')[0];
        this.slider = new Slider(this._el.getElementsByClassName('slider')[0]);
        this.slider.setListener(color => {
            this.setColor(color);

            if (this._listener) {
                this._listener(color);
            }
        });

        // Initiate
        this._btn_lock.addEventListener('click', e => this._lockUnlock(e.target));
        this._btn_adjust.addEventListener('click', e => this.slider.adjust(this.color));
    }

    _lockUnlock(el) {
        if (el.classList.toggle('locked')) {
            el.innerHTML = '<i class="fas fa-lock"></i>';
        } else {
            el.innerHTML = '<i class="fas fa-lock-open"></i>';
        }
    }

    /**
     * Check if the color is locked or not
     * 
     * @returns bool
     */
    _isLocked() {
        return this._btn_lock.classList.contains('locked');
    }

    /**
     * Set listener
     * 
     * @param {function} fn 
     */
    setListener(fn) {
        this._listener = fn;
    }

    /**
     * To adjust the current color
     * 
     * @param {chroma} color 
     */
    setColor(color) {
        this.color = color;

        this._txt_color.textContent = color.hex().toUpperCase();
        this._el.style.backgroundColor = color.hex();

        if (color.luminance() > 0.5) {
            this._el.classList.remove('light-color');
        } else {
            this._el.classList.add('light-color');
        }
    }

    /**
     * 
     * @param {boolean} ignore_locked 
     * @returns 
     */
    generateRandomColor(ignore_locked = true) {
        this.slider.close();
        if (ignore_locked && this._isLocked()) return;
        this.setColor(chroma.random());
    }
}

class Popup {
    /**
     * Create new instance
     */
    constructor() {
        this._el = document.createElement('div');
        this._el.className = 'popup-wrapper';

        this._window = document.createElement('div');
        this._window.className = 'popup';

        this._close = document.createElement('span');
        this._close.className = 'close';
        this._close.textContent = 'X';

        this._window.appendChild(this._close);
        this._window.appendChild(this.getContent());
        this._el.appendChild(this._window);
        document.body.appendChild(this._el);

        // Initiate
        this._close.addEventListener('click', e => this.close());
    }

    getContent() {
        return document.createElement('div');
    }

    open() {
        this._el.classList.add('show');
    }

    close() {
        this._el.classList.remove('show');
        document.body.removeChild(this._el);
    }
}

class CopyPopup extends Popup {
    getContent() {
        const copy_popup = document.createElement('div');
        copy_popup.className = 'popup-copy';
        copy_popup.innerHTML = `<div class="popup-copy"><p>Copied to clipboard!</p><span><i class="far fa-thumbs-up"></i></span></div>`;

        return copy_popup;
    }
}

class SavePopup extends Popup {
    constructor() {
        super();
        this._save_btn = this._el.getElementsByClassName('btn-save')[0];
        this._input = this._el.getElementsByClassName('input_name')[0];
    }

    getContent() {
        const save_popup = document.createElement('div');
        save_popup.className = 'popup-save';
        const header = document.createElement('h2');
        header.textContent = 'Give a name to your palette!';
        const input = document.createElement('input');
        input.type = 'text';
        input.className = 'input_name';
        const button = document.createElement('button');
        button.className = 'btn-save';
        button.textContent = 'Save';

        save_popup.appendChild(header);
        save_popup.appendChild(input);
        save_popup.appendChild(button);

        return save_popup;
    }

    askName(fn) {
        this.open();
        this._save_btn.addEventListener('click', e => {
            this.close();
            fn(this._input.value);
        });
    }
}

class LibraryPopup extends Popup {
    /**
     * 
     * @param {string} name 
     * @param {[string]} colors 
     */
    _createNewPalette(name, colors) {
        const palette = document.createElement('div');
        palette.className = 'palette';

        const title = document.createElement('span');
        title.textContent = name;
        palette.appendChild(title);

        const pick = document.createElement('div');
        pick.className = 'pick';
        colors.forEach(color => {
            const span = document.createElement('span');
            span.style.backgroundColor = color;
            pick.appendChild(span);
        });
        const button = document.createElement('button');
        button.textContent = 'Select';
        button.className = 'btn-select';
        button.$colors = colors;
        pick.appendChild(button);

        palette.appendChild(pick);

        return palette;
    }

    getContent() {
        const library_popup = document.createElement('div');
        library_popup.className = 'popup-library';
        const title = document.createElement('h2');
        title.textContent = 'Pick your palette';
        library_popup.appendChild(title);

        for (let [name, colors] of Object.entries(ColorStorage.getPalettes())) {
            library_popup.appendChild(this._createNewPalette(name, colors));
        }

        return library_popup;
    }

    choosePalette(fn) {
        this.open();
        const buttons = this._el.querySelectorAll('.btn-select');
        buttons.forEach(button => button.addEventListener('click', e => {
            this.close();
            fn(e.target.$colors);
        }));

    }
}

class ColorStorage {
    /**
     * 
     * @param {[string]} name 
     * @param {[chroma]} colors 
     */
    static storePalette(name, colors) {
        let palettes = {};
        if (localStorage.getItem('palettes')) {
            palettes = JSON.parse(localStorage.getItem('palettes'));
        }

        let colors_hex = [];
        colors.forEach(color => {
            colors_hex.push(color.color.hex());
        });

        palettes[name] = colors_hex;
        localStorage.setItem('palettes', JSON.stringify(palettes));
    }

    static getPalettes() {
        if (!localStorage.getItem('palettes')) return {};

        return JSON.parse(localStorage.getItem('palettes'));
    }
}

class Palette {
    constructor(el) {
        this._el = el;
        this._colors = [];

        this._el.querySelectorAll('.color').forEach(el => {
            let color = new Color(el);
            this._colors.push(color);
        });
    }

    /**
     * Refresh unlocked color(s) on the palette
     */
    refreshPalette() {
        this._colors.forEach(color => {
            color.generateRandomColor();
        });
    }

    savePalette() {
        const save_popup = new SavePopup();
        save_popup.askName(name => {
            ColorStorage.storePalette(name, this._colors);
        });
    }

    selectPalette() {
        const library_popup = new LibraryPopup();
        library_popup.choosePalette(colors => {
            colors.forEach((color, index) => {
                this._colors[index].setColor(chroma(color));
            });
        });
    }
}

const palette = new Palette(document.getElementById('palette'));
palette.refreshPalette();

// Selectors
const btn_library = document.getElementById('btn-library');
const btn_refresh = document.getElementById('btn-refresh');
const btn_save = document.getElementById('btn-save');

// Listener
btn_library.addEventListener('click', () => palette.selectPalette());
btn_refresh.addEventListener('click', () => palette.refreshPalette());
btn_save.addEventListener('click', () => palette.savePalette());