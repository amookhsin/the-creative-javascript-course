// Selectors
const todoInput = document.querySelector("#todo-input");
const todoButton = document.querySelector("#todo-button");
const todoList = document.querySelector("#todo-list");
const todoFilters = document.querySelector("#todo-filters");


// Event Listeners
document.addEventListener('DOMContentLoaded', updateTodoList);
todoButton.addEventListener('click', addTodo);
todoFilters.addEventListener('click', filterHandler);

// Functions
function addTodo(event) {
    // Prevent form from submitting
    event.preventDefault();

    const todo = todoInput.value;

    if (!saveToLocalStorage(todo)) {
        return;
    }

    creatTodoElement(todo);
}

function creatTodoElement(todo) {
    // Todo div
    const todoDiv = document.createElement("div");
    todoDiv.classList.add("todo");

    // Create li
    const newTodo = document.createElement("li");
    newTodo.classList.add("todo-item");
    newTodo.innerText = todo;

    // Create CheckMark Button
    const completeButton = document.createElement("button");
    const checkIcon = document.createElement("i");
    completeButton.classList.add("todo-btn", "complete");
    checkIcon.classList.add("fas", "fa-check");
    completeButton.appendChild(checkIcon);
    completeButton.addEventListener('click', completeHandler);

    // Create Delete Button
    const deleteButton = document.createElement("button");
    const deleteIcon = document.createElement("i");
    deleteButton.classList.add("todo-btn", "delete");
    deleteIcon.classList.add("fas", "fa-trash");
    deleteButton.appendChild(deleteIcon);
    deleteButton.addEventListener('click', deleteHandler);

    // Append to todoDiv
    todoDiv.appendChild(newTodo);
    todoDiv.appendChild(completeButton);
    todoDiv.appendChild(deleteButton);

    // Append todoDiv to todo-list
    todoList.appendChild(todoDiv);
}


function deleteHandler(event) {
    const todo = event.target.parentElement;

    todo.childNodes.forEach(function (nod) {
        if (nod.tagName === 'LI') {
            deleteFromLocalStorage(nod.innerText);
        }
    });

    todo.remove();
}

function completeHandler(event) {
    const todo = event.target.parentElement;
    todo.classList.toggle("complete");
}

function filterHandler(event) {
    const todos = todoList.childNodes;

    todos.forEach(function (todo) {
        if (todo.classList === undefined || todo.classList.length < 1) {
            return;
        }

        switch (event.target.value) {
            case 'all':
                todo.style.display = 'flex';
                break;
            case 'completed':
                if (todo.classList.contains('complete')) {
                    todo.style.display = 'flex';
                } else if (todo.classList.contains('todo')) {
                    todo.style.display = 'none';
                }
                break;
            case 'uncompleted':
                if (todo.classList.contains('complete')) {
                    todo.style.display = 'none';
                } else if (todo.classList.contains('todo')) {
                    todo.style.display = 'flex';
                }
                break;
        }
    });
}

function updateTodoList() {
    let todos;

    if (localStorage.getItem('todos') === null) {
        return;
    }

    todos = JSON.parse(localStorage.getItem('todos'));

    todos.forEach(function (todo) {
        creatTodoElement(todo);
    });
}

function saveToLocalStorage(todo) {
    let todos;

    if (localStorage.getItem('todos') === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem('todos'));
    }

    if (todos.includes(todo)) {
        return false;
    }

    todos.push(todo);

    localStorage.setItem('todos', JSON.stringify(todos));

    return true;
}

function deleteFromLocalStorage(todo) {
    let todos;

    if (localStorage.getItem('todos') === null) {
        return;
    }

    todos = JSON.parse(localStorage.getItem('todos'));

    if (todos.includes(todo)) {
        todos.splice(todos.indexOf(todo), 1);

        localStorage.removeItem('todos');
        localStorage.setItem('todos', JSON.stringify(todos));
    }
}