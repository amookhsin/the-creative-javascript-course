# پایه

بهتره فایل جاواسکریپت در پایان تگ body وارد شود تا همه‌ی عنصرهای html شناختیده باشند.

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <script src="./app.js"></script>
</body>

</html>
```

## بیانوشتن (comment)

از `//` برای بیانوشتن تک‌خطی و از <span dir="ltr" markdown="1">`/* */`</span> برای بیانوشتن چندخطی می‌استفیم:

```js
// the number of inbox items
let inbox = 20;

/**
 * the number of email items
 * the number can't be greater than 200
 */
let email = 20;
```

## متغیر و ثابت (variables and constants)

یه متغیر با کلیدواژه‌ی `let`  و یه ثابت با کلیدواژه `const` تعریفیده می‌شود:

```js
let varName1;
let varName2 = 'Ali';

const constName = 100;
```

اشاریدن به یه متغیر/ثابت:

```js
varName2 = 'Change Value';

alert(varName1);
alert(constName);
```

**نکته.** یه ثابت حتما باید هنگام تعریف‌اش مقداردهی شود؛ چون نمی‌توان مقدارش را غیرید.

**قرارداد.** متغیرها/ثابت‌ها به‌شکل camelCase نامیده شوند.

### حوزه‌ی تعریف متغیرها/ثابت‌ها

هر متغیری که با کلیدواژه `let` اعلانیده شود، تنها در حوزه تعریف خودش و حوزه‌های لانیده در خودش دردسترس است. و اگر در حوزه‌ی دستورها با کلیدواژه `var`، در حوزه‌ی سراسری اعلانیده می‌شوند.

**توجه.** هر پودمان (فایل) تنها یه حوزه‌ی سراسری دارد و حوزه‌های دیگر با `{}` تعیینده می‌شود.

**توجه.** جاواسکریپت برای یافتن یه متغیر، حوزه‌ی جاری به بالا را می‌جوید.

**توجه.** بهتره هیچ متغیری را با کلیدواژه `var` ناعلانیم.

## عملگرها

### عملگرهای سنجشی

<div dir="rtl" align="right" markdown="1">

* `==`: سنجیدن هم‌ارزی
* `===`: سنجیدن هم‌ارزی و همسانی
* `!=`: سنجیدن ناهم‌ارزی
* `<`: سنجیدن کوچکتر
* `>`: سنجیدن بزرگتر
* `<=`: سنجیدن کوچکتر یا مساوی
* `>=`: سنجیدن بزرگتر یا مساوی

</div>

### عملگرهای منطقی

<div dir="rtl" align="right" markdown="1">

* `&&`: «و» منطقی
* `||`: «یا» منطقی

</div>

### عملگرهای ریاضی

<div dir="rtl" align="right" markdown="1">

* `+`: جمع 
* `-`: تفریق
* `*`: ضرب
* `/`: تقسیم
* `**`: توان
* `%`: عملگر پیمانه‌ای

</div>

#### عملگرهای ریاضی انتسابی

متغیر هم به‌عنوان عملوند درنظر گرفته می‌شود و هم به عنوان ظرف نتیجه

<div dir="rtl" align="right" markdown="1">

* `+=`: <span dir="ltr" markdown="1">`a += b`</span>
* `-=`: <span dir="ltr" markdown="1">`a -= b`</span>
* `*=`: <span dir="ltr" markdown="1">`a *= b`</span>
* `/=`: <span dir="ltr" markdown="1">`a /= b`</span>
* `**`: <span dir="ltr" markdown="1">`a **= b`</span>
* `%`: <span dir="ltr" markdown="1">`a %= b`</span>
* `++`: افزایش یک واحد
* `--`: کاستن یک واحد

</div>

## عبارت‌های تصمیم‌گیری

هنگامیکه بخواهیم یه همیده دستور تنها در شرایطی اجراییده شوند.

### ارزش درستی/نادرستی

در جاواسکریپت، همانند پایتون، شی‌ها دارای ارزش درستی/نادرستی هستند. مانند، همه عددها مگر صفر که دارای ارزش `false` است، دارای ارزش `true` هستند.

شی‌های دارای ارزش نادرستی:

* `0`: عدد صفر
* `""`: رشته‌ی خالی
* `null`: هیچ
* `undefined`: تعریفیده نشده
* `NaN`: ناعدد

### عبارت شرطی `if-else`

اجراییدن یه همیده دستور هنگام درستی شرط:

```js
// ...
if (condition) {
    // if condition is true
} else if (condition_2){
    // if condition is false and condition_2 is true
} else {
    // if condition and condition_2 are false
}
// ...
```

### عبارت `switch`

تصمیمیدن براساس مقدار یه متغیر:

```js
switch(v) {
    case 'val 1':
    // Some code
    break; // to exit of switch statement
    case 'val 2':
    // Some code
    break;
    default:
    // Some code
}
```

## حلقه‌ی تکرار

برای تکراریدن یه همیده از کد تا هنگام درستی عبارت ویا به‌ازای عنصرهای یه مجموعه.

### حلقه تکرار `while`

برای تکراریدن یه همیده کد تا هنگام درستی شرط حلقه:

```js
while (condition) {
    // Run some code
}
```

### حلقه‌ی تکرار `do-while`

اجراییدن یه همیده کد و تکراریدن تا درستی شرط حلقه.

```js
do {
    // Run some code
} while(condition);
```

### حلقه تکرار `for`

همانند حلقه‌ی تکرار `while` با تفاوت امکان تعریفیدن وضعیت نخستین و گام حلقه:

```js
for (let i; i < 10; i++) {
    // Some code
    
    // to jump over to the next iteration
    if (condition) {
        continue;
    }

    // to stop running this for loop.
    if (condition) {
        break;
    }
}
```

### حلقه‌ی تکرار `foreach`

تکراریدن یه همیده کد به ازای عنصرهای یه فراهمیک (collection):

```js
let names = ['Name 1', 'Name 2', 'Name 3']

// supported in some explorer
// To iterate through an array
for (let name of names) {
    // Run some code

    // to jump over to the next iteration
    if (condition) {
        continue;
    }

    // to stop running this for loop.
    if (condition) {
        break;
    }
}

const user = {
    name: 'User Name',
    age: 25,
    subscribers: 200,
    money: 'lolno'
}
// To iterate through an object
for (let key in user) {
    // Run some code
}


// Specifically for arrays
names.forEach(function (name, index){
    // Run some code
});
```

## تابع (function)

بازاستفپذیراندن به همیده از دستورها. تعریفیدن:

```js
function func_name(arg) {
    // cods
}
```

فراخواندن:

```js
func_name(val);
```

**توجه.** ترتیب تعریفیدن تابع‌های وابسته به‌هم تا پیش از فراخوانده شدن مهم نیست.

```js
function signUp() {
    greet();
    // ...
}
function greet() {
    // ...
}

signUp();
```

**توجه.** همه‌ی تابع‌ها شی `undefined` را می‌برگردانند.

### پارامتر با مقدار پیشفرض

با اعلانیدن مقدار پیشفرض برای یه پارامتر می‌توان فرستادن یه آرگومان را اختیاری کرد.

```js
function func(arg = 0) {
    // ...
}
```

## آرایه (Array)

یه ساختمان‌داده پساریکی برای نگهداری یه دسته داده که ممکنه ناهمگن باشند.

**اعلانیدن.**

```js
let names = ['Name 1', 'Name 2', 'Name 3']
let arr = [1, '2']
```

**دسترسی به عنصرها.** با استفیدن از ایندکس.

```js
names[1]
```

## رشته

### استفیدن از بیاننده (expression) در رشته

درون template literal می‌توان بیاننده نیز نوشت.

```js
const name = 'Amookhsin';
const greeting = `Hello, ${name}`;
```

### پیوستاندن دو رشته به‌هم

از عملگر `+` برای پیوستاندن (concatenate) دو رشته به‌هم می‌استفیم.

```js
const greeting = "Hello, ";
const name = "Ali";
const say_hello = greeting + name;
```

## چاپیدن مقدار در خروجی

با استفیدن از  تابع `console.log` می‌توان در پایانه مرورگر چیزی چاپید:

```js
console.log('Hello world!');
```
