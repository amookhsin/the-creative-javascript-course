# شی (Objects)

## اجرایش دستورها

مفسر جاواسکریپت در دو مرحله دستورها را می‌اجراید. در مرحله نخست، ساخت، تابع‌ها و متغیرهای محلی تعریفیده می‌شوند. و در مرحله دوم، اجرا، دستورهای برنامه خط‌به‌خط اجراییده می‌شوند. این باعث می‌شود که یه تابع را بتوان پیش از تعریفش، فراخواند.

```js
sayHi();

function sayHi(){
    console.log('Hi there!');
}
```

## کلیدواژه `this`

این کلیدواژه در وضعش سراسری به شی `window` می‌اشارد و در وضعش تابع‌ها وابسته به چگونگی (سختگیرانه، غیرسختگیرانه) فراخوانی تابع‌اِ است. در سختگیرانه باید نهاده شود وگرنه به `undefined` می‌اشارد. ولی در غیرسختگیرانه به گره سراسری می‌اشارد. ولی در ES2015 برای نهادن مقدار `this` صرفنظر از چگونگی فراخوانی تابع، متد <span dir="ltr" markdown="1">`bind()`</span> ارایه شده.

```js
function f1() {
  // non-strict mode  
  return this;
}

// In a browser:
f1() === window; // true
// In Node:
f1() === globalThis; // true

function f2() {
  'use strict';
  return this;
}

f2() === undefined;
```

**نکته.** در جاواسکریپت `this` در تابع‌های، معمولی اگر بعنوان یه خصیصه یا متد یه شی فراخوانده شوند، به آن شی‌اِ می‌اشارد. ولی در تابع‌های پیکانی `this` به شی‌اِ دوسیده نمی‌شود.

```js
const test = {
  prop: 42,
  func: function() {
    return this.prop;
  },
};

console.log(test.func()); // expected output: 42
```

### دوسیدن یه شی به کلیدواژه `this` در تابع

برای ساختن یه تابع جدید که درونش کلیدواژه `this` به یه شی خاص دوسیده باشد، از متد `bind` می‌استفیم.

```js
const person = {
  first_name: 'First Name',
  last_name: 'Last Name'
};

function registerUser(){
  console.log(this.first_name);
  console.log(this.last_name);
}

// bind(thisArg, arg1, ... , argN)
register = registerUser.bind(person);

register()
```

ولی اگر بخواهیم یه تابع را بفراخوانیم طوریکه کلیدواژه `this` به یه شی خاص باشارد، از متد `call` می‌استفیم.

```js
// call(thisArg, arg1, ... , argN)
registerUser.call(person);
```

هنگامی هم که بخواهیم یه آرایه بعنوان آرگومان به یه تابع بفرستیم می‌توانیم از متد `apply` باستفیم.

```js
// apply(thisArg, argsArray)
registerUser(person)
```

## شیگراییدن

در جاواسکریپت به دو روش می‌توان شیگرایید.

* استفیدن از تابع سازنده
* استفیدن از کلاس

### تابع سازنده (Constructor Functions)

در جاواسکریپت می‌توان تابعی تعریفیدن که وظیفه‌اش ساخت یه نمونه مشخص است. طبق قرارداد این تابع‌های به شکل `FuncName` نامیده می‌شوند.

```js
function Todo(title, completed) {
  this.title = title;
  this.completed = completed;

  this.getTitle = function() {
    return this.title;
  }
}

const todo = new Todo('Buy Eggs', false);
```

#### افزودن خصیصه به `prototype`

تمام شی‌های جاواسکریپت خصیصه‌های `prototype` را می‌ارثبرند. برای همین برای افزودن خصیصه‌های مشترک به همه‌ی شی‌های یه نوع خاص آن را به `prototype` آن نوع می‌افزاییم تا حافظه‌ی کمتری اشغال شود.

```js
function Todo(title, completed) {
  this.title = title;
  this.completed = completed;
}

Todo.prototype.getTitle = function() {
    return this.title;
  }
```

#### ارثبردن از یه سازنده

برای ارثبردن از یه سازنده، نخست باید آن سازنده با `this` شی جدید فراخوانده شود؛ سپس از `prototype`اش هم ارثبرده شود.

```js
function Person(name, age) {
  this.name = name;
  this.age = age;
}

Person.prototype.getName = function() {
  return this.name;
}
Person.prototype.getAge = function() {
  return this.age;
}

function Student(name, age, std_num) {
  // keyword this is going reference the Dragon object
  Person.call(this, name, age);
  // Define it as normal
  this.std_name = std_name;
}

// Inherit prototype
Student.prototype = Object.create(Person.prototype);
// Define a new method as normal
Student.prototype.getStdNum = function() {
  return this.std_num;
}
```

**توجه.** این <span dir="ltr" markdown="1">`Student.prototype = Object.create(Person.prototype);`</span> باعث می‌شود یه شی `prototype` ساخته شود و یه زنجیره ایجاد شود.

![زنجیره prototype](assets/prototype-chain.png)

### کلاس

در نسخه‌های جدید جاواسکریپت کلیدواژه `class` برای افزودن امکان شیگرایی به زبان افزوده شده. ولی درعمل خروجی همانند تابع سازنده است که مفسر جاواسکریپت بشکل خودکار می‌انجامد.

**تعریفیدن کلاس.**

```js
class Person {
  constructor (name, age) {
    this.name = name;
    this.age = age;
  }

  getName() {
    return this.name;
  }

  getAge() {
    return this.age;
  }
}
```

**ساختن نمونه کلاس.**

```js
p = new Person('Amookhsin', 1);
```

#### ارثبردن

از کلیدواژه `extends` برای ارثبردن از یه کلاس استفیده می‌شود.

```js
class Student extends Person {
  constructor(name, age, std_num) {
    super(name, age, std_num);
    this.std_num = std_num;
  }

  getStudentNumber() {
    return this.std_num;
  }
}
```
