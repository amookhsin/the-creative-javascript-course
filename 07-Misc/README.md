# چند ترفند

## واساختن شی‌های پساریکی (Deconstruct)

در جاواسکریپت هم مانند پایتون می‌توان شی‌ها و آرایه‌ها را واساخت (واهمید).

```js
let array = [1, 2, 3, 4, 5]
let [a, b, ...x] = array;

let obj = {
    name: 'Amookhsin',
    age: 1,
    photos: [1, 2, 3, 4, 5]
}
let {name} = obj;
```

## فراخواندن آنی تابع (IFFE)

در جاواسکریپت می‌توان بدون نامش بشکل زیر فراخواند:

```js
(function sayHollo(name){
    console.log(`Hello ${name}`);
})();
```

**توجه.** این ترفند برای مواقعی هم که بخواهیم حوزه‌ی متغیرها را بمنزویم کاربرد دارد.

```js
(function library_name(){
    const var_name = 'value';
    //...
})();
```

## بستار (closure)

هنگامیکه اجرای تابع پایانده می‌شود، حوزه‌ی متغیرهایش از پشته حذف می‌شود. ولی بدلیل وجود بستار تابع درونی یه تابع حتی پس از پایان اجرای تابع بیرونی، به متغیرهای تابع بیرونی دسترسی دارد.

```js
function user() {
    name = 'Amookhsin';

    const displayName = function(greeting) {
        console.log(greeting + name);
    }

    return displayName;
}

const say = user();
say('How are you? ');
```
