# آرایه (Arrays)

در جاواسکریپت دو نوع نوع-داده وجود دارد:

1. نوع-داده‌های اصلی (primitive data types): مقداری مشخص در حافظه‌اند که متغیرها نمودنده‌ی آن‌ها هستند.
2. نوع-داده‌های ارجاعی (reference data types): شی‌هایی که حافظه‌اند که متغیرها بهشان می‌ارجاعند.

**نکته.** هنگام انتسابیدن متغیرها بهم، اگر متغیر انتسابیده اصلی باشد، مقدارش در متغیر جدید کپیده می‌شود، وگرنه نشانی شی‌ای که می‌ارجاعد.

**توجه.** نوع-داده‌های ارجاعی ممکنه دگرشپذیر ویا دگرشناپذیر باشند.

## تابع‌های مرتبه-بالا (higher order functions)

در جاواسکریپت هرچیزی، غیر از نوع-داده‌های اصلی، شی است؛ حتی تابع‌ها. تابعی که یه تابع بعنوان ورودی می‌گیرد، تابع مرتبه-بالا، و تابعی که به یه تابع مرتبه-بالا فرستاده می‌شود، تابع `callback` خوانده می‌شود.

```js
function repeater(fn) {
    fn();
    fn();
}

function sayHello() {
    console.log("Hello There!");
}

repeater(sayHello);
```

### انواع تابع

در جاواسکریپت سه نوع تابع:

* بانام: `function func_name(){}`
* بی‌نام (anonymous): `caller(function () {})`
* پیکانی (arrow function)

وجود دارد.

**توجه.** تابع‌های بی‌نام و پیکانی معمولا بعنوان callback استفیده می‌شوند.

```js
let sayHello = function (){
    console.log("Hello There!");
}

let sayHi = function (){
    console.log("Hi There!");
}

let sayHiTo = name => {
    console.log('Hi ' + name);
}

repeater(function (){
    console.log("Hello There!");
})
```

## تکراریدن بر همه‌ی عضوهای یه آرایه

برای تکراریدن بر همه‌ی عضوهای یه آرایه از متد `forEach` کلاس آرایه می‌استفیم.

```js
const videos = ["Video1", "Video2", "Video3"];

videos.forEach(function (video){
    console.log(video);
});
```

## نگاشتن یه مقدار به مقداری دیگر

برای نگاشتن یه مقدار به مقدار دیگر، از متد `map` کلاس آرایه می‌استفیم.

```js
const videos = ["Video1", "Video2", "Video3"];

const newVideos = videos.map(function (video){
    return video.toUpperCase();
});
```

## جوییدن یه عضو در یه آرایه

برای جوییدن نخستین عضو یه آرایه که در یه شرط صادق است، از متد `find` کلاس آرایه می‌استفیم.

```js
const videos = ["Video1", "Video2", "Video3"];

const searchVideo3 = videos.find(function (video){
    return video.toLowerCase() === "video3";
});
```

## سواییدن عضوهایی از یه آرایه

برای سواییدن عضوهای یه آرایه درون یه آرایه دیگر، از متد `filter` کلاس آرایه می‌استفیم.

```js
const videos = ["Video1", "Video2", "Video3"];

const specialVideos = videos.filter(function (video){
    return video.toLowerCase() === "video3" || video.toLowerCase() === "video1";
});
```

## بررسیدن یه شرط به‌ازای همه‌ی عضوهای آرایه

برای بررسیدن راستی یه شرط به‌ازای همه‌ی عضوهای یه آرایه از متد `every`، و برای بررسیدن راستی یه شرط به‌ازای حداقل یه عضو از متد `some` کلاس آرایه می‌استفیم.

```js
const videos = ["Video1", "Video2", "Video3"];

const checkLength = videos.filter(function (video){
    return video.length >= 6;
});

const check2Suffix = videos.some(function (video){
    return video. charAt(video. length-1) === "1";
});
```

## رتبیدن عنصرهای آرایه

برای رتبیدن عنصرهای یه آرایه (افزایشی) از متد `sort` کلاس آرایه می‌استفیم؛ ولی باید درنظر داشت که این متد بشکل پیشفرض نخست عضوهای آرایه را به رشته می‌بدلد سپس می‌اندازسنجد. برای تغییر این رفتار می‌توان تابع سنجه را بهش داد.

در مثال زیر، اگر تابع سنجه (<span dir="ltr" markdown="1">$`a - b`$</span>):

* یه عدد کوچکتر از صفر برگرداند، <span dir="ltr" markdown="1">$`a < b`$</span>
* صفر برگرداند، <span dir="ltr" markdown="1">$`a = b`$</span>
* یه عدد بزرگتر از صفر برگرداند، <span dir="ltr" markdown="1">$`a > b`$</span>

تعیینیده می‌شود.

```js
let ratings = [92, 10, 2, 3, 20, 18];

rating.sort((a, b) => a - b);
```

**توجه.** این متد بر خود شی می‌اثرد.

**توجه.**‌ برای رتبیدن آرایه بشکل کاهشی می‌توان نتیجه تابع سنجه را قرینید.

## ناهمیدن شی‌های تکرارپذیر (Spread)

در جاواسکریپت، برای ناهمیدن شی‌های تکرارپذیر از عملگر `...` می‌استفیم.

```js
const l1 = [1, 2, 3]
const l2 = [4, 5, 6]

const l3 = [...l1, ...l2]
```
